<!DOCTYPE html>
<html lang="ca">
 <head>
  <meta cahrset="utf-8">
  <title>PHP Taula de multiplicar</title>
  <link rel="stylesheet" href="./practica2.css">
 </head>
 <body>
	<p><h1> TAULA DE MULTIPLICAR </h1></p>
	<p> Taula de les multiplicacions fins a 10*10, fet per Óscar Borrego Rusticus</p>
 <table>
	<table border="5px" align="center">
  <?php
   echo "<tr class='parell'>";
   echo "<th colspan=11>";
   echo "TAULA";
   echo "</th> </tr>";
   echo"<tr class='impar'>"; 
  for($cabecera="0";$cabecera<=10;$cabecera++){
 	echo "<th>";
	echo $cabecera;
	echo "</th>";
  }
  echo "</tr>";
  for($x=1;$x<=10;$x++){
	echo "<tr>";
	echo "<th class='impar'>";
	echo $x;
	echo "</th>";
	 for($y = 1; $y<=10;$y++){
		$multiplicacion=$x*$y;
		echo "<td>";
		echo $multiplicacion;
		echo"</td>";
	}
	echo "</tr>";
  }
  ?>
 </table>
 <footer>Óscar Borrego Rusticus, 1r DAM-Vi. </footer>
 </body>
</html>
